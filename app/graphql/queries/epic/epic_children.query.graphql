fragment PageInfo on PageInfo {
  hasNextPage
  hasPreviousPage
  startCursor
  endCursor
}

fragment RelatedTreeBaseEpic on Epic {
  id
  iid
  title
  webPath
  relativePosition
  userPermissions {
    __typename
    adminEpic
    createEpic
  }
  descendantWeightSum {
    closedIssues
    openedIssues
  }
  descendantCounts {
    __typename
    openedEpics
    closedEpics
    openedIssues
    closedIssues
  }
  healthStatus {
    __typename
    issuesAtRisk
    issuesOnTrack
    issuesNeedingAttention
  }
}

fragment EpicNode on Epic {
  ...RelatedTreeBaseEpic
  state
  reference(full: true)
  relationPath
  createdAt
  closedAt
  confidential
  hasChildren
  hasIssues
  group {
    __typename
    id
    fullPath
  }
}

query childItems(
  $fullPath: ID!
  $iid: ID
  $pageSize: Int = 100
  $epicEndCursor: String = ""
  $issueEndCursor: String = ""
) {
  group(fullPath: $fullPath) {
    __typename
    id
    path
    fullPath
    epic(iid: $iid) {
      __typename
      ...RelatedTreeBaseEpic
      children(first: $pageSize, after: $epicEndCursor) {
        __typename
        edges {
          __typename
          # We have an id in deeply nested fragment
          # TODO: Uncomment next line when https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75220 is merged and the rule is enabled
          # # eslint-disable-next-line @graphql-eslint/require-id-when-available
          node {
            __typename
            ...EpicNode
          }
        }
        pageInfo {
          __typename
          ...PageInfo
        }
      }
      issues(first: $pageSize, after: $issueEndCursor) {
        __typename
        edges {
          __typename
          node {
            __typename
            id
            iid
            epicIssueId
            title
            blocked
            closedAt
            state
            createdAt
            confidential
            dueDate
            weight
            webPath
            reference(full: true)
            relationPath
            relativePosition
            assignees {
              __typename
              edges {
                __typename
                node {
                  __typename
                  id
                  webUrl
                  name
                  username
                  avatarUrl
                }
              }
            }
            milestone {
              __typename
              id
              title
              startDate
              dueDate
            }
            healthStatus
          }
        }
        pageInfo {
          __typename
          ...PageInfo
        }
      }
    }
  }
}
